$(document).ready(function () {

    listar(1);
    //listarContacto();

    $(".filtros select[name=estado]").bind("change", function () {
        var est = $(this).val();
        listar(est);
    });

    /*new Request("tipo_proyecto/listar/",{
        estado: 1
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.pry_id_web+'">'+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         //listar();
    });*/

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                listar(1);
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar(res.estado);
                $(".filtros select[name=estado]").val(res.estado);
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    /*html.find(".btn-editar").click(function () {
        alert("HOLA");
        $("#modaleditar").modal("show");
    });*/

});



function listar(est) {
    console.log('log');
    $("#tipo .lista").empty();

    new Request("tipo_proyecto/listar/", {
        estado: est
    }, function (res) {
        $("#tipo .lista").empty();
        if(res != "empty"){
            $.each(res, function (k, v) {
                var it = new ItemTipo(v);
                $("#tipo .lista").append(it);
            })
        }else{
            alert("No existen tipo proyectos en este estado.");
        }

    });


}


var ItemTipo = function (data) {

    var id = data.tip_id;
    var descripcion = data.tip_descripcion;
    var estado = data.tip_estado;

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + descripcion + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-outline-primary btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
        '</div></td>' +
        '</tr>');

        /*    var html = $('<tr width="100%" data-id="' + id + '">' +
                    '<td>' + data.pry_titulo + '</td>' +
                    '<td>' + data.pry_orden + '</td>' +
                    '<td><div class="btn-group" role="group" aria-label="...">' +
                    '<button type="button" class="btn btn-outline-primary btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
                    '</div></td>' +
                '</tr>');*/



        html.find(".btn-editar").click(function () {

            $("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=tipoproyecto]").val(descripcion);
            $("#modaleditar select[name=estado]").val(estado);

            $("#modaleditar").modal("show");

            $("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("tipo_proyecto/eliminar/" + id, {
                    }, function (res) {
                        listar(1);
                        $("#modaleliminar").modal("hide");
                    });
                });

            })


        });

    return html;
};


