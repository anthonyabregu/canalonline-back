<?php
	class Administrador_model extends CI_Model{

		function __construct(){

			parent::__construct();
			$this->load->library('session');
		}

		public function login($usuario, $clave){

			if(isset($usuario) && isset($clave)){

              /*$this->db->select('*');
              $this->db->from('usuario');
              $this->db->where('usr_email', $usuario);
              $this->db->where('usr_pass', $clave);
              $this->db->where('usr_estado', 1);*/

              $this->db->select('usr.usr_id as usr_id, GROUP_CONCAT(rls.fnc_id ORDER BY rls.fnc_id ASC) AS roles');
              $this->db->from('usuario as usr');
              $this->db->join('roles as rls', 'usr.usr_id = rls.usr_id');
              $this->db->where('usr.usr_email', $usuario);
              $this->db->where('usr.usr_pass', $clave);
              $this->db->where('usr.usr_estado', 1);
              $this->db->group_by("usr.usr_id");


              // Obtenemos el resultado de la consulta
              $resultado = $this->db->get();
              if($resultado->num_rows() == 1) {
                $r = $resultado->row(); // guarda el registro de la consulta

                $roles = explode(',', $r->roles);         

                $s_usuario =  array('idUsuario' => $r->usr_id,
                                   'tipoUsuario' => $roles,
                               	   'Conexion' => TRUE);

                //Ponemos en session
                $this->session->set_userdata($s_usuario);

                $result = array('respuesta' => 1);

              }else{
                  $result = array('respuesta' => 'Usuario y/o contraseña errónea.');
              }
          	}else{
              	$result = array('respuesta' => 'NO HUBO ENVÍO DE PARÁMETROS');

          	}

          	return $result;
		}

	} 
?>