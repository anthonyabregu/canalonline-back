<?php

	class Proyecto_model extends CI_Model{

		function __construct(){

			parent::__construct();
		}

        public function listar($estado,$tipo){
          
            $sql = 'SELECT pry.*, ubg.* FROM proyecto as pry inner join ubigeo as ubg on pry.ubg_id = ubg.ubg_id 
                    where pry.pry_estado= ? and pry.tip_id= ? ORDER BY pry_id ASC';

            $query = $this->db->query($sql, array($estado, $tipo));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }

		public function listado($estado, $usr_id, $usr_tipo, $rol){

            if(in_array(1, $usr_tipo)){

                $sql = "SELECT * from proyecto";

            }else{

                if(in_array(2, $usr_tipo)){

                    //$tipousuario = implode(",", $usr_tipo);

                    $sql = "SELECT pry.* from proyecto as pry inner join roles as rls 
                            on pry.pry_id = rls.pry_id WHERE rls.usr_id= ?
                            AND rls.fnc_id in (?) ORDER BY pry.pry_id ASC";
                }

                   
            }

            $query = $this->db->query($sql, array($usr_id, $rol ));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }

        public function agregar($param){
         
          return $this->db->insert('proyecto', $param);

        }

        public function editar($param, $id){
    
            $this->db->where('pry_id', $id);
            $result = $this->db->update('proyecto', $param);

            return $result;
        }


        public function eliminar($pry_id){
            $this->db->where('pry_id', $pry_id);
            $result = $this->db->delete('proyecto');

            return $result;
        }



	}
?>