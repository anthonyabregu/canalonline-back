<?php
    class Contacto_model extends CI_Model{

        function __construct(){
            parent::__construct();
        }

        public function listar($usr_id, $usr_tipo){

            if($usr_tipo == 1){

                //Query para mostrar los contactos
                $sql = "SELECT cto.cnt_id as 'id', cto.cnt_nombre as 'nombre',cto.cnt_dni as 'dni',cto.cnt_celular as 'celular',
                        cto.cnt_email as 'email',cto.cnt_mensaje as 'mensaje', cto.cnl_id as 'canal', pry.pry_descripcion as 'proyecto',
                        pry.pry_id as 'idproyecto', cto.cnt_registro as 'registro' from contacto as cto inner join proyecto as pry 
                        on cto.pry_id = pry.pry_id ORDER BY cto.cnt_registro DESC";

                $query = $this->db->query($sql);

            }else{

                // Query para hallar la cantidad de contactos (Encargado Tipo 2 ó N)
                $sqlcant = "SELECT count(*) as 'cont' from contacto as cto inner join proyecto as pry on cto.pry_id = pry.pry_id 
                            inner join usuario_proyecto as up on pry.pry_id = up.pry_id WHERE up.usr_id = ? 
                            ORDER BY cto.cnt_registro DESC";

                $querycant = $this->db->query($sqlcant, array($usr_id));

                $cantrow = $querycant->first_row();

                $res["total"] = $cantrow->cant;

                //Query para mostrar los contactos
                $sql = "SELECT cto.cnt_id as 'id', cto.cnt_nombre as 'nombre',cto.cnt_dni as 'dni',cto.cnt_celular as 'celular',
                        cto.cnt_email as 'email',cto.cnt_mensaje as 'mensaje', cto.cnl_id as 'canal', pry.pry_descripcion as 'proyecto',
                        pry.pry_id as idproyecto, cto.cnt_registro as 'registro' from contacto as cto inner join proyecto as pry 
                        on cto.pry_id = pry.pry_id inner join usuario_proyecto as up on pry.pry_id = up.pry_id WHERE up.usr_id = ?
                        ORDER BY cto.cnt_registro DESC";

                $query = $this->db->query($sql, array($usr_id));
            }

            //$query = $this->db->query($sql);

            if($query->num_rows()>0){
                $res["data"] = $query->result();
                return $res;
            }else{
                return FALSE;
            }

        }

        public function agregar($param){
          $campos = array(
            'cnt_nombre' => $param['cnt_nombre'],
            'cnt_dni' => $param['cnt_dni'],
            'cnt_celular' => $param['cnt_celular'],
            'cnt_email' => $param['cnt_email'],
            'pry_id' => $param['pry_id'],
            'cnt_mensaje' => $param['cnt_mensaje'],
            'cnl_id' => $param['cnl_id'],
            'cnt_term' => $param['cnt_term']
          );

          return $this->db->insert('contacto', $campos);

        }

        public function editar($param, $id){
            $campos = array(
                'cnt_nombre' => $param['cnt_nombre'],
                'cnt_dni' => $param['cnt_dni'],
                'cnt_celular' => $param['cnt_celular'],
                'cnt_email' => $param['cnt_email'],
                'pry_id' => $param['pry_id'],
                'cnt_mensaje' => $param['cnt_mensaje'],
                'cnl_id' => $param['cnl_id']
            );

            $this->db->where('cnt_id', $id);
            $result = $this->db->update('contacto', $campos);

            return $result;
        }


        public function usuario_proyecto($pry_id, $rol){

            $sql = "SELECT usr.usr_nombres as 'nombres', usr.usr_email as 'email', pry.pry_id as 'id_proyecto',
                    pry.pry_descripcion as 'proyecto' from usuario as usr inner join roles as rls on usr.usr_id = rls.usr_id
                    inner join proyecto as pry on rls.pry_id = pry.pry_id WHERE pry.pry_id = ? and rls.fnc_id = ? and usr.usr_estado = ? ";

            $query = $this->db->query($sql, array($pry_id, $rol, 1));

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }


        public function exportar_csv($pry_id,$s_rango){

            $tipoUsuario = $this->session->userdata('tipoUsuario');
            $idUsuario = $this->session->userdata('idUsuario');

            $rango = $this->formatearRango($s_rango);
            
            $rango_ini = $rango["ini"];
            $rango_fin = $rango["fin"];

            //var_dump($pry_id);

            if(in_array(1, $tipoUsuario)){

                if($pry_id == ""){

                    $this->db->select('cto.cnt_nombre,cto.cnt_dni,cto.cnt_celular,cto.cnt_email,cto.cnt_mensaje,cto.cnl_id,
                                   pry.pry_descripcion, cto.cnt_registro');
                    $this->db->from('contacto as cto');
                    $this->db->join('proyecto as pry', 'cto.pry_id = pry.pry_id');
                    $this->db->where("DATE(cto.cnt_registro) BETWEEN '$rango_ini' AND '$rango_fin'");
                    $this->db->order_by("cto.cnt_registro", "desc");

                }if($pry_id != ""){

                    $this->db->select('cto.cnt_nombre,cto.cnt_dni,cto.cnt_celular,cto.cnt_email,cto.cnt_mensaje,cto.cnl_id,
                                   pry.pry_descripcion, cto.cnt_registro');
                    $this->db->from('contacto as cto');
                    $this->db->join('proyecto as pry', 'cto.pry_id = pry.pry_id');
                    $this->db->where('cto.pry_id', $pry_id);
                    $this->db->where("DATE(cto.cnt_registro) BETWEEN '$rango_ini' AND '$rango_fin'");
                    $this->db->order_by("cto.cnt_registro", "desc");
                }

                

            }else{

                if($pry_id == ""){

                    if(in_array(2, $tipoUsuario)){

                        $this->db->select('cto.cnt_nombre,cto.cnt_dni,cto.cnt_celular,cto.cnt_email,cto.cnt_mensaje,cto.cnl_id,
                                   pry.pry_descripcion, cto.cnt_registro');
                        $this->db->from('contacto as cto');
                        $this->db->join('proyecto as pry', 'cto.pry_id = pry.pry_id');
                        $this->db->join('roles as rls', 'pry.pry_id = rls.pry_id');
                        $this->db->where('rls.usr_id', $idUsuario);
                        $this->db->where('rls.fnc_id in (2)'); 
                        $this->db->where("DATE(cto.cnt_registro) BETWEEN '$rango_ini' AND '$rango_fin'");
                        $this->db->order_by("cto.cnt_registro", "desc");
                    }
                
                    

                
                } else{
                    $this->db->select('cto.cnt_nombre,cto.cnt_dni,cto.cnt_celular,cto.cnt_email,cto.cnt_mensaje,cto.cnl_id,
                                   pry.pry_descripcion, cto.cnt_registro');
                    $this->db->from('contacto as cto');
                    $this->db->join('proyecto as pry', 'cto.pry_id = pry.pry_id');
                    $this->db->join('roles as rls', 'pry.pry_id = rls.pry_id');
                    $this->db->where('cto.pry_id', $pry_id);
                    $this->db->where('rls.fnc_id in (2)'); 
                    $this->db->where('rls.usr_id', $idUsuario);
                    $this->db->where("DATE(cto.cnt_registro) BETWEEN '$rango_ini' AND '$rango_fin'");
                    $this->db->order_by("cto.cnt_registro", "desc");
                }
            }

             
               
            return $this->db->get();

            
        }

        public function listarPorFiltros($pry_id, $usr_id, $usr_tipo, $s_rango, $limit){

            $rango = $this->formatearRango($s_rango);

            //var_dump($usr_tipo);

            $tipousuario = implode(",", $usr_tipo);
            
            $rango_ini = $rango["ini"];
            $rango_fin = $rango["fin"];

            if(in_array(1, $usr_tipo)){

                // Query para hallar la cantidad de contactos (Encargado Tipo 1)
                $sqlcant = "SELECT count(*) as cant from contacto as cto inner join proyecto as pry 
                            on cto.pry_id = pry.pry_id WHERE pry.pry_id = ? AND DATE(cto.cnt_registro) BETWEEN ? AND ?
                            ORDER BY cto.cnt_registro DESC ";

                $querycant = $this->db->query($sqlcant, array($pry_id, $rango_ini, $rango_fin));

                $cantrow = $querycant->first_row();

                $res["total"] = $cantrow->cant;

                //Query para mostrar los contactos
                $sql = "SELECT cto.cnt_id as 'id', cto.cnt_nombre as 'nombre',cto.cnt_dni as 'dni',cto.cnt_celular as 'celular',
                        cto.cnt_email as 'email',cto.cnt_mensaje as 'mensaje', cto.cnl_id as 'canal', pry.pry_descripcion as 'proyecto',
                        pry.pry_id as idproyecto, cto.cnt_registro as 'registro' from contacto as cto inner join proyecto as pry 
                        on cto.pry_id = pry.pry_id WHERE pry.pry_id = $pry_id AND DATE(cto.cnt_registro) BETWEEN '$rango_ini' AND '$rango_fin'
                        ORDER BY cto.cnt_registro DESC ".$limit;

                $query = $this->db->query($sql, array($pry_id, $rango_ini, $rango_fin));        

            }else{

                if(in_array(2, $usr_tipo)){

                    // Query para hallar la cantidad de contactos (Encargado Tipo 2 ó N)
                    $sqlcant = "SELECT count(*) as cant from contacto as cto inner join proyecto as pry on cto.pry_id = pry.pry_id 
                                inner join roles as rls on pry.pry_id = rls.pry_id WHERE pry.pry_id = ? AND rls.usr_id= ? 
                                AND rls.fnc_id in (2) AND DATE(cto.cnt_registro) BETWEEN ? AND ?
                                ORDER BY cto.cnt_registro DESC " .$limit;

                    $querycant = $this->db->query($sqlcant, array($pry_id, $usr_id, $rango_ini, $rango_fin));

                    $cantrow = $querycant->first_row();

                    $res["total"] = $cantrow->cant;

                    //Query para mostrar los contactos
                    $sql = "SELECT cto.cnt_id as 'id', cto.cnt_nombre as 'nombre',cto.cnt_dni as 'dni',cto.cnt_celular as 'celular',
                            cto.cnt_email as 'email',cto.cnt_mensaje as 'mensaje', cto.cnl_id as 'canal', pry.pry_descripcion as 'proyecto',
                            pry.pry_id as idproyecto, cto.cnt_registro as 'registro' from contacto as cto inner join proyecto as pry 
                            on cto.pry_id = pry.pry_id inner join roles as rls on pry.pry_id = rls.pry_id WHERE pry.pry_id = $pry_id 
                            AND rls.usr_id= $usr_id AND rls.fnc_id in (2) AND DATE(cto.cnt_registro) BETWEEN '$rango_ini' AND '$rango_fin' 
                            ORDER BY cto.cnt_registro DESC " .$limit;

                    $query = $this->db->query($sql, array($pry_id, $usr_id, $rango_ini, $rango_fin));
                }

                
            }

            //$query = $this->db->query($sql);

            if($query->num_rows()>0){
                $res["data"] = $query->result();
                return $res;
            }else{
                return FALSE;
            }
        }

        public function listarPorFecha($usr_id, $usr_tipo, $s_rango, $limit){

            $rango = $this->formatearRango($s_rango);
            
            $rango_ini = $rango["ini"];
            $rango_fin = $rango["fin"];

            //var_dump($usr_tipo);

            $tipousuario = implode(",", $usr_tipo);

            //var_dump($tipousuario);

            if(in_array(1, $usr_tipo)){

                // Query para hallar la cantidad de contactos (Encargado Tipo 1)
                $sqlcant = "SELECT count(*) as cant from contacto WHERE DATE(cnt_registro) BETWEEN ? AND ? ";
                $querycant = $this->db->query($sqlcant, array($rango_ini, $rango_fin));

                $cantrow = $querycant->first_row();

                $res["total"] = $cantrow->cant;

                //Query para mostrar los contactos
                $sql = "SELECT cto.cnt_id as 'id', cto.cnt_nombre as 'nombre',cto.cnt_dni as 'dni',cto.cnt_celular as 'celular',
                        cto.cnt_email as 'email',cto.cnt_mensaje as 'mensaje', cto.cnl_id as 'canal', pry.pry_descripcion as 'proyecto',
                        pry.pry_id as idproyecto, cto.cnt_registro as 'registro' from contacto as cto inner join proyecto as pry 
                        on cto.pry_id = pry.pry_id WHERE DATE(cto.cnt_registro) BETWEEN '$rango_ini' AND '$rango_fin'
                        ORDER BY cto.cnt_registro DESC ".$limit;

                $query = $this->db->query($sql, array($rango_ini, $rango_fin));

            }else{

                if(in_array(2, $usr_tipo)){
                     // Query para hallar la cantidad de contactos (Encargado Tipo 2 ó N)
                    $sqlcant = "SELECT count(*) as cant from contacto as cto inner join proyecto as pry 
                            on cto.pry_id = pry.pry_id inner join roles as rls on pry.pry_id = rls.pry_id WHERE rls.usr_id= ? 
                            AND rls.fnc_id in (2) AND DATE(cto.cnt_registro) BETWEEN ? AND ? 
                            ORDER BY cto.cnt_registro DESC ".$limit;

                    $querycant = $this->db->query($sqlcant, array($usr_id, $rango_ini, $rango_fin));

                    $cantrow = $querycant->first_row();

                    $res["total"] = $cantrow->cant;

                    //Query para mostrar los contactos
                    $sql = "SELECT cto.cnt_id as 'id', cto.cnt_nombre as 'nombre',cto.cnt_dni as 'dni',cto.cnt_celular as 'celular',
                            cto.cnt_email as 'email',cto.cnt_mensaje as 'mensaje', cto.cnl_id as 'canal', pry.pry_descripcion as 'proyecto',
                            pry.pry_id as 'idproyecto', cto.cnt_registro as 'registro' from contacto as cto inner join proyecto as pry 
                            on cto.pry_id = pry.pry_id inner join roles as rls on pry.pry_id = rls.pry_id WHERE rls.usr_id= $usr_id 
                            AND rls.fnc_id in (2) AND DATE(cto.cnt_registro) BETWEEN '$rango_ini' AND '$rango_fin' 
                            ORDER BY cto.cnt_registro DESC ".$limit;

                    $query = $this->db->query($sql, array($usr_id, $rango_ini, $rango_fin));
                }

               
            }

            if($query->num_rows()>0){
                $res["data"] = $query->result();
                return $res;
            }else{
                return FALSE;
            }
        }


        public function dashboard_contacto($s_rango){

            if($s_rango == ""){

                $sql = "SELECT pry.pry_descripcion as 'proyecto', count(*) as 'cantidad',
                    (SELECT SUM(contactos) FROM (SELECT COUNT(*) as contactos 
                     FROM contacto GROUP BY pry_id) as contactos) as 'total'
                    from contacto as cto inner join proyecto as pry on cto.pry_id = pry.pry_id 
                    GROUP BY cto.pry_id ORDER BY cantidad DESC";

                $query = $this->db->query($sql);

            }else{

                $rango = $this->formatearRango($s_rango);

                //var_dump($rango);

                $rango_ini = $rango["ini"];
                $rango_fin = $rango["fin"];

                $sql = "SELECT cto.cnt_registro, pry.pry_descripcion as 'proyecto', count(*) as 'cantidad',
                (SELECT SUM(contactos) FROM (SELECT COUNT(*) as contactos FROM contacto 
                 WHERE DATE(cnt_registro) BETWEEN ? AND ? GROUP BY pry_id) as contactos) as 'total', 
                 cto.cnt_registro from contacto as cto inner join proyecto as pry on cto.pry_id = pry.pry_id 
                 WHERE DATE(cto.cnt_registro) BETWEEN ? AND ? GROUP BY cto.pry_id  ORDER BY cantidad DESC";

                $query = $this->db->query($sql, array($rango_ini, $rango_fin, $rango_ini, $rango_fin ));
            }
          

            $query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
        }



        public function formatearRango($rango){
            $rango = explode(" - ",$rango);
            $res["ini"] = implode("-",array_reverse(explode("/",$rango[0])));
            $res["fin"] = implode("-",array_reverse(explode("/",$rango[1])));

            return $res;
        }

           



    }
?>
