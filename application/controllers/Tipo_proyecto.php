<?php

  use Restserver\Libraries\REST_Controller;
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . '/libraries/Format.php';

	class Tipo_proyecto extends REST_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model('Tipo_proyecto_model', 'tipo');
		}

		
		public function listar_get(){

			$estado = intval($this->input->get('estado'));

			$lista = $this->tipo->listar($estado);
        	
        	if($lista != false){
        		$res = $lista;
        		$this->response($res);
        	}else{
        		$res = "empty";
        		$this->response($res);
        	}
		}


		public function agregar_post(){

          $param['tip_descripcion'] = $this->input->post('tipoproyecto');


          $result = $this->tipo->agregar($param);

          if($result != false){
            $res["res"] = "ok";
            //$res["estado"] = $param['ctg_estado'];
            $this->response($res);
          }
        }

        public function actualizar_post(){

            $id = intval($this->input->post('id'));

            $param['tip_descripcion'] = $this->input->post('tipoproyecto');
            $param['tip_estado'] = $this->input->post('estado');
          	

            $editar = $this->tipo->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
    		      $res["estado"] = $param['tip_estado'];
    		      $this->response($res);
            }
        }


        public function eliminar_get($id){
            
            /*if(!$this->session->has_userdata('admin')){
              exit();
            }*/
            
            $this->tipo->eliminar($id);


            $res["res"] = "ok";
            $this->response($res);
        }

	}
?>