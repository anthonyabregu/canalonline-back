<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


    public function __construct(){
        parent::__construct();
        $this->load->model('Administrador_model','admin');
        $this->load->library('session');
        //$this->load->library('email');
    }


    /*public function index()
    {

        $this->versesion();

         $data = array(
            "page" => "home"
        );

        $this->load->view('admin/header', $data);
        $this->load->view('admin/home');
        $this->load->view('admin/footer');

    }

    private function versesion(){

        if(!$this->session->has_userdata('Conexion') &&
           !$this->session->has_userdata('idUsuario') &&
           !$this->session->has_userdata('tipoUsuario')){
            redirect('admin/login');
            exit();
        }
    }

    public function login()
    {
        $this->load->view('admin/login');
    }

    public function designar($usr_id){
		
		$data = array(
			"encargado" => $usr_id,
			"page" => "designar"
		);

		$this->load->view('admin/header',$data);
		$this->load->view('admin/designar_proyecto');
		$this->load->view('admin/footer');
	}

	public function contacto(){
        $this->versesion();

        $data = array(
                "page" => "contacto"
            );

        $this->load->view('admin/header', $data);
        $this->load->view('admin/contacto');
        $this->load->view('admin/footer');
    }

    public function proyectos(){
        $this->versesion();

		$data = array(
          "page" => "proyecto"
        );

		$this->load->view('admin/header', $data);
        $this->load->view('admin/proyectos');
        $this->load->view('admin/footer');
    }

    public function tipo_proyecto(){
        $this->versesion();

        $data = array(
            "page" => "tipoproyecto"
        );

		$this->load->view('admin/header', $data);
        $this->load->view('admin/tipo_proyecto');
        $this->load->view('admin/footer');
    }

    public function Encargado(){
        $this->versesion();

        $data = array(
           "page" => "encargado"
        );

		$this->load->view('admin/header', $data);
        $this->load->view('admin/encargado');
        $this->load->view('admin/footer');
    }


    public function usuariowsp(){
        $this->versesion();

        $data = array(
           "page" => "usuariowsp"
        );

        $this->load->view('admin/header', $data);
        $this->load->view('admin/usuariowsp');
        $this->load->view('admin/footer');
    }

    public function designarwsp($usr_id){

        $this->versesion();
        
        $data = array(
            "usuario" => $usr_id,
            "page" => "designarwsp"
        );

        $this->load->view('admin/header',$data);
        $this->load->view('admin/designarwsp');
        $this->load->view('admin/footer');
    }

    public function origen_referencia(){

        $this->versesion();
        
        $data = array(
            "page" => "origen"
        );

        $this->load->view('admin/header',$data);
        $this->load->view('admin/origen_referencia');
        $this->load->view('admin/footer');
    }

    public function referenciado($id){

        $this->versesion();
        
        $data = array(
            "referenciado" => $id,
            "page" => "referenciado"
        );

        $this->load->view('admin/header',$data);
        $this->load->view('admin/referenciado');
        $this->load->view('admin/footer');
    }

    public function designar_encargado($id){

        $this->versesion();
        
        $data = array(
            "proyecto" => $id,
            "page" => "designar_encargado"
        );

        $this->load->view('admin/header',$data);
        $this->load->view('admin/designar_encargado');
        $this->load->view('admin/footer');
    }

    public function referido(){
        
        $this->versesion();

        $data = array(
                "page" => "referido"
            );

        $this->load->view('admin/header', $data);
        $this->load->view('admin/referido');
        $this->load->view('admin/footer');
    }*/

}
