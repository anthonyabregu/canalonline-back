﻿<?php

    use Restserver\Libraries\REST_Controller;
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . '/libraries/Format.php';

    class Contacto extends REST_Controller{

        function __construct(){
            parent::__construct();
            $this->load->model('Contacto_model', 'contacto');
            /*$this->load->library('REST_Controller');
            $this->load->library('Format');*/

        }

        public function listar_get(){

            $pry_id = intval($this->input->get("pry_id"));
            $usr_id = intval($this->input->get("usr_id"));
            //$usr_tipo = intval($this->input->get("usr_tipo"));
            $usr_tipo = $this->input->get("usr_tipo");
            $s_rango = $this->input->get("rango");

            //var_dump($usr_tipo);

            $pg = $this->input->get("pg");
            $cant = $this->input->get("cant");

            $limit = "";
            if($pg!=0 && $cant!=0){
                $ini = ($pg - 1)*$cant;
                $limit = " LIMIT ".$ini.",".$cant;
            }

            if($pry_id == ""){

                $data = $this->contacto->listarPorFecha($usr_id, $usr_tipo, $s_rango, $limit);

            }else{
                $data = $this->contacto->listarPorFiltros($pry_id, $usr_id, $usr_tipo, $s_rango, $limit);
                
            }

            //$lista = $this->contacto->listar_contactos($pry_id, $usr_id, $usr_tipo, $s_rango);

            if($data != false){
                $res["res"] = "ok";
                $res["lista"] = $data["data"];
                $res["total"] = $data["total"];
            }else{
                $res["res"] = "failed";
            }

             $this->response($res);
        }

        public function agregar_post(){
          
            $json = $this->post('par');
            //var_dump($json);

            $param['ctn_nombre'] = $json["nombre"];
            $param['ctn_dni'] = $json["dni"];
            $param['ctn_celular'] = $json["celular"];
            $param['ctn_email'] = $json["email"];
            $param['pry_id'] = intval($json["proyecto"]);
            $param['ctn_mensaje'] = $json["mensaje"];
            $param['cnl_id'] = $json["canal"];
            $param['ctn_term'] = intval($json["terminos"]);
            $param['ctn_politica'] = intval($json["politica"]);

            
            $rolusuario = 2;


            //DESIGNAR CORREO A RECEPTORES
            $usuario = $this->contacto->usuario_proyecto($param['pry_id'], $rolusuario);

            //var_dump($usuario);

            if($usuario != false){
              foreach($usuario as $user){
                  $correo = $this->enviar($param, $user);
              }
            }else{
                var_dump("NO SE ENCONTRO USUARIO RECEPTOR");
            }

            $result = $this->contacto->agregar($param);

            if($result != false){
                $res["res"] = "ok";
                //$res["estado"] = $param['ctg_estado'];
                 $this->response($res);
            }
        }

        public function actualizar(){

            $id = intval($this->input->post('id'));
            $param['ctn_nombre'] = $this->input->post('nombre');
            $param['ctn_dni'] = $this->input->post('dni');
            $param['ctn_celular'] = $this->input->post('celular');
            $param['ctn_email'] = $this->input->post('email');
            $param['pry_id'] = intval($this->input->post('proyecto'));
            $param['ctn_mensaje'] = $this->input->post('mensaje');
            $param['cnl_id'] = $this->input->post('canal');

            $editar = $this->contacto->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
  		        //$res["estado"] = $param['ctg_estado'];
  		       $this->response($res);
            }
        }

        public function enviar($param, $user){

            /*$nombres = $this->input->post("nombres");
            $dni = $this->input->post("dni");
            $celular = $this->input->post("celular");
            $email = $this->input->post("email");
            $mensaje = $this->input->post("mensaje");
            $proyecto = $this->input->post("proyecto");*/

            $fechahora = date("Y-m-d H:i:s");

            $data = array(
                "nombre" => $param['ctn_nombre'],
                "dni" => $param['ctn_dni'],
                "celular" => $param['ctn_celular'],
                "email" => $param['ctn_email'],
                "mensaje" => $param['ctn_mensaje'],
                "proyecto" => $user->proyecto,
                "registro" => $fechahora
            );

            //$this->contacto->agregar($data);

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'contacto@menorca.com.pe',
                'smtp_pass' => 'menorca123',
                'mailtype'  => 'html',
                'charset'   => 'utf-8'
            );
            $this->load->library('email', $config);

            $this->email->initialize($config);
            
            $this->email->set_newline("\r\n");

            // Set to, from, message, etc.
            $this->email->from('contacto@menorca.com.pe', "Menorca");

            $this->email->to($user->email);


            $this->email->reply_to($user->email);


            $this->email->subject($user->proyecto." | menorca.pe - Coordina tu visita");


            $txt_mensaje = '
                <style>
                    table tr td{
                        background-color:#fff;
                        font-family:Tahoma;
                        font-size:12px
                    }
                </style>
                <table width="400" border="0" cellpadding="10" cellspacing="1" bgcolor="#ccc">
                    <tr>
                        <td colspan="2" bgcolor="#fff"><center><strong>MENSAJE DE CONTACTO DESDE LA WEB</strong></center></td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>Proyecto:</strong></td>
                        <td bgcolor="#fff">'.$user->proyecto.'</td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>Nombres</strong></td>
                        <td bgcolor="#fff">'.$data["nombre"].'</td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>DNI:</strong></td>
                        <td bgcolor="#fff">'.$data["dni"].'</td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>Tel&eacute;fono:</strong></td>
                        <td bgcolor="#fff">'.$data["celular"].'</td>
                    </tr>
                    <tr>
                        <td bgcolor="#fff"><strong>Email:</strong></td>
                        <td bgcolor="#fff">'.$data["email"].'</td>
                    </tr>

                    <tr>
                        <td bgcolor="#fff"><strong>Mensaje</strong>:</td>
                        <td bgcolor="#fff">'.str_replace("\n","<br>",$data["mensaje"]).'</td>
                    </tr>
                </table>';


            $this->email->message($txt_mensaje);

            $result = $this->email->send();

            //return $user->email . "" .$txt_mensaje;


            if($result != false){
              $res["envio_correo"] = "ok";
            }else{
              $res["envio_correo"] = "failed";
            }

            return $res;


        }

        public function dashboard_contacto_get(){


            $_rango = $this->input->get("rango");

            $lista = $this->contacto->dashboard_contacto($_rango);

            if($lista != false){
              $res["res"] = "ok";
              $res["lista"] = $lista;
            }else{
              $res["res"] = "failed";
            }

             $this->response($res);
        }

    }
?>
