<?php

	use Restserver\Libraries\REST_Controller;
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . '/libraries/Format.php';

	class Ubigeo extends REST_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model('Ubigeo_model', 'ubigeo');
		}

    	function listar_get(){
	      $lista = $this->ubigeo->listar();
	      $this->response($lista);
    	}

	}
?>