<?php

  use Restserver\Libraries\REST_Controller;
    require APPPATH . '/libraries/REST_Controller.php';
    require APPPATH . '/libraries/Format.php';

	class Proyecto extends REST_Controller{

		function __construct(){
			parent::__construct();
			$this->load->model('Proyecto_model', 'proyecto');
		}

		
		public function listado_get(){

			$estado = intval($this->input->get('estado'));
      $usr_id = intval($this->input->get('usr_id'));
      $rol = intval($this->input->get('rol'));

      $usr_tipo= $this->input->get('usr_tipo'); // array

			$lista = $this->proyecto->listado($estado, $usr_id, $usr_tipo, $rol);
        	
        	if($lista != false){
        		$res = $lista;
        		$this->response($res);
        	}else{
        		$res = "empty";
        		$this->response($res);
        	}
		}

		public function listar_get(){

          /*if(!$this->session->has_userdata('admin')){
  			       exit();
          }*/

          $estado = intval($this->input->get("estado"));
          $tipo = intval($this->input->get("tipo"));


      		$lista = $this->proyecto->listar($estado,$tipo);
          

          if( $lista != false )
          {
            $this->response($lista);
          }
          else{
            $res = 'empty';
            $this->response($lista);
          }

        }


		public function agregar_post(){

          $param['pry_descripcion'] = $this->input->post('proyecto');
          $param['tip_id'] = intval($this->input->post('tipoproyecto'));
          $param['ubg_id'] = intval($this->input->post('ubigeo'));


          $result = $this->proyecto->agregar($param);

          if($result != false){
            $res["res"] = "ok";
            //$res["estado"] = $param['ctg_estado'];
            $this->response($res);
          }
        }

        public function actualizar_post(){

            $id = intval($this->input->post('id'));

            $param['pry_descripcion'] = $this->input->post('proyecto');
	        $param['tip_id'] = intval($this->input->post('tipoproyecto'));
	        $param['ubg_id'] = intval($this->input->post('ubigeo'));
            $param['pry_estado'] = intval($this->input->post('estado'));
          	

            $editar = $this->proyecto->editar($param, $id);

            if($editar != false){
              $res["res"] = "ok";
  		        $res["estado"] = $param['pry_estado'];
  		        $this->response($res);
            }
        }


        public function eliminar_get($id){
            
            /*if(!$this->session->has_userdata('admin')){
              exit();
            }*/
            
            $this->proyecto->eliminar($id);


            $res["res"] = "ok";
            $this->response($res);
        }

	}
?>